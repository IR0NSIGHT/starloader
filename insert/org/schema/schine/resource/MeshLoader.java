//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package org.schema.schine.resource;

import api.mod.StarMod;
import api.mod.annotations.DoesNotWork;
import org.apache.commons.io.IOUtils;
import org.schema.common.util.data.DataUtil;
import org.schema.common.util.data.ResourceUtil;
import org.schema.schine.graphicsengine.animation.structure.classes.AnimationStructure;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.ResourceException;
import org.schema.schine.graphicsengine.forms.AbstractSceneNode;
import org.schema.schine.graphicsengine.forms.Mesh;
import org.schema.schine.graphicsengine.forms.MeshGroup;
import org.schema.schine.graphicsengine.meshimporter.XMLOgreParser;
import org.schema.schine.graphicsengine.texture.Texture;

import java.io.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class MeshLoader {
    public static boolean loadVertexBufferObject = true;
    private final Map<String, Mesh> meshMap = new HashMap();
    private final Map<String, Texture> textureMap = new HashMap();

    public MeshLoader(ResourceUtil var1) {
    }

    public boolean loadMesh(String var1, String var2, String var3, AnimationStructure var4, TextureStructure var5, CreatureStructure var6, String var7) throws ResourceException {
        return this.addMesh(var1, var2, var3, var4, var5, var6, var7, false);
    }
    private static String getModNamespace(StarMod mod, String name){
        return mod.getName() + "~" + name;
    }

    //INSERTED CODE
    public Mesh getModMesh(StarMod mod, String name){
        return getMeshMap().get(getModNamespace(mod, name));
    }
    public boolean loadModMesh(StarMod mod, String name, InputStream zippedFile, String physicsMesh) throws ResourceException, IOException {
        System.err.println("Loading Mod mesh...");
        ZipInputStream zip = new ZipInputStream(zippedFile);

        String dir = "models/modmodels/"+mod.getName() + "/" + name + "/"; //second / has to be here for materials to work
        String outputDir = "data/" + dir;
        new File(outputDir).mkdirs();
        while (true) {
            ZipEntry entry = zip.getNextEntry();
            if (entry == null) {
                break;
            }
            File entryDestination = new File(outputDir, entry.getName());
            if (entry.isDirectory()) {
                entryDestination.mkdirs();
            } else {
                entryDestination.getParentFile().mkdirs();
                OutputStream out = new FileOutputStream(entryDestination);
                //Apparently getNextEntry will position the start at the next entry, then handle EOF by itself,
                // So we will see if this works
                IOUtils.copy(zip, out);
            }
        }
        zip.close();
        System.err.println("Mod mesh copied");

        addMesh(getModNamespace(mod, name), name, dir, null, null, null, physicsMesh, false);
        System.err.println("Added mod mesh");
        return true;
    }
    @DoesNotWork
    public void loadModObjMesh(StarMod mod, String name){
        try {
            Mesh mesh = Mesh.loadObj("display_screen.obj");
            this.getMeshMap().put(getModNamespace(mod, name), mesh);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    ///

    private boolean addMesh(String name, String fileName, String path, AnimationStructure var4, TextureStructure var5, CreatureStructure var6, String physicsMesh, boolean overwrite) throws ResourceException {
        System.err.println("ADDED MESH: " + name + ", filname: " + fileName + ", path: " + path + ", phys: " + physicsMesh);
        try {
            if (!overwrite && this.getMeshMap().containsKey(name)) {
                return true;
            } else {
                XMLOgreParser var10000 = new XMLOgreParser();
                var4 = null;
                MeshGroup var11;
                var11 = var10000.parseScene(DataUtil.dataPath + path + File.separator, fileName);
                var11.setName(name);
                Iterator var12;
                AbstractSceneNode var13;
                if (loadVertexBufferObject) {
                    var12 = var11.getChilds().iterator();

                    label68:
                    while(true) {
                        do {
                            if (!var12.hasNext()) {
                                break label68;
                            }
                        } while(!((var13 = (AbstractSceneNode)var12.next()) instanceof Mesh));

                        try {
                            while(!var13.isLoaded()) {
                                Mesh.buildVBOs((Mesh)var13);
                                if (physicsMesh != null && physicsMesh.equals("convexhull")) {
                                    ((Mesh)var13).loadPhysicsMeeshConvexHull();
                                }

                                if (physicsMesh != null && physicsMesh.equals("dedicated")) {
                                    ((Mesh)var13).retainVertices();
                                }
                            }
                        } catch (Exception var9) {
                            System.err.println("error in " + name);
                            var9.printStackTrace();
                        }
                    }
                } else {
                    var12 = var11.getChilds().iterator();

                    while(var12.hasNext()) {
                        if ((var13 = (AbstractSceneNode)var12.next()) instanceof Mesh && physicsMesh != null && physicsMesh.equals("dedicated")) {
                            ((Mesh)var13).retainVertices();
                        }
                    }
                }

                GlUtil.LOCKDYN = false;
                GlUtil.locked.clear();
                if (!ResourceLoader.dedicatedServer) {
                    this.assignMaterials(var11, path);
                }

                this.getMeshMap().put(name, var11);
                return true;
            }
        } catch (ResourceException var10) {
            var10.printStackTrace();
            return false;
        }
    }

    private void assignMaterials(Mesh var1, String var2) throws ResourceException {
        if (var1.getMaterial().isMaterialTextured()) {
            if (var1.getMaterial().getTextureFile() != null) {
                try {
                    var1.getMaterial().texturePathFull = DataUtil.dataPath + var2 + var1.getMaterial().getTextureFile();
                    Texture var3;
                    if ((var3 = (Texture)this.textureMap.get(var1.getMaterial().texturePathFull)) == null) {
                        var3 = Controller.getTexLoader().getTexture2D(var1.getMaterial().texturePathFull, true);
                        this.textureMap.put(var1.getMaterial().texturePathFull, var3);
                    }

                    var1.getMaterial().setTexture(var3);
                    String var4 = var1.getMaterial().getTextureFile().replace(".", "_normal.");
                    String var5 = var1.getMaterial().getTextureFile().replace(".", "_specular.");
                    if ((new File(DataUtil.dataPath + var2 + var4)).exists()) {
                        var1.getMaterial().normalTexturePathFull = DataUtil.dataPath + var2 + var4;
                        if ((var3 = (Texture)this.textureMap.get(var1.getMaterial().normalTexturePathFull)) == null) {
                            var3 = Controller.getTexLoader().getTexture2D(var1.getMaterial().normalTexturePathFull, true);
                            this.textureMap.put(var1.getMaterial().normalTexturePathFull, var3);
                        }

                        var1.getMaterial().setNormalMap(var3);
                        var1.getMaterial().setMaterialBumpMapped(true);
                    }

                    if ((new File(DataUtil.dataPath + var2 + var5)).exists()) {
                        var1.getMaterial().specularTexturePathFull = DataUtil.dataPath + var2 + var5;
                        if ((var3 = (Texture)this.textureMap.get(var1.getMaterial().specularTexturePathFull)) == null) {
                            var3 = Controller.getTexLoader().getTexture2D(var1.getMaterial().specularTexturePathFull, true);
                            this.textureMap.put(var1.getMaterial().specularTexturePathFull, var3);
                        }

                        var1.getMaterial().setSpecularMap(var3);
                        var1.getMaterial().setSpecularMapped(true);
                    }
                } catch (IOException var8) {
                    System.err.println("ERROR LOADING: " + DataUtil.dataPath + var2 + var1.getMaterial().getTextureFile());
                    var8.printStackTrace();
                }
            }

            if (var1.getMaterial().getEmissiveTextureFile() != null) {
                try {
                    var1.getMaterial().setEmissiveTexture(Controller.getTexLoader().getTexture2D(DataUtil.dataPath + var2 + var1.getMaterial().getEmissiveTextureFile(), true));
                } catch (IOException var7) {
                    var7.printStackTrace();
                }
            }

            if (!var1.getMaterial().isMaterialBumpMapped() && var1.getMaterial().getNormalTextureFile() != null) {
                try {
                    String var12 = DataUtil.dataPath + var2 + var1.getMaterial().getNormalTextureFile();
                    Texture var9 = Controller.getTexLoader().getTexture2D(var12, true);

                    assert var9 != null;

                    var1.getMaterial().setNormalMap(var9);
                    var1.getMaterial().setMaterialBumpMapped(true);

                    assert var1.getMaterial().getNormalMap() != null;
                } catch (IOException var6) {
                    var6.printStackTrace();
                }
            }
        }

        Iterator var13 = var1.getChilds().iterator();

        while(var13.hasNext()) {
            AbstractSceneNode var10;
            if ((var10 = (AbstractSceneNode)var13.next()) instanceof Mesh) {
                Mesh var11 = (Mesh)var10;
                this.assignMaterials(var11, var2);
            }
        }

    }

    public Map<String, Mesh> getMeshMap() {
        return this.meshMap;
    }
}
