package org.schema.schine.graphicsengine.psys;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectList;
import it.unimi.dsi.fastutil.objects.ObjectListIterator;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector4f;
import org.schema.schine.graphicsengine.camera.CameraMouseState;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.Transformable;
import org.schema.schine.network.client.ClientState;
import org.schema.schine.network.objects.container.TransformTimed;
import org.schema.schine.physics.Physics;

import javax.swing.*;
import javax.vecmath.Matrix3f;
import javax.vecmath.Vector3f;
import java.awt.*;
import java.util.Collections;
import java.util.Iterator;

public class ParticleSystemManager implements ISortableParticles {
    static Vector4f res = new Vector4f();
    public ObjectList<ParticleSystem> systems = new ObjectArrayList();

    public ParticleSystemManager() {
    }

    public void startParticleSystemWorld(ParticleSystemConfiguration var1, Transform var2) {
        assert var2 != null;

        ParticleSystem var3 = new ParticleSystem(var1, new ParticleSystemManager.WorldTransformable(var2));
        this.systems.add(var3);
        var3.start();
    }

    public void stopParticleSystemsWorld() {
        Iterator var1 = this.systems.iterator();

        while(var1.hasNext()) {
            ((ParticleSystem)var1.next()).stop();
        }

    }

    public void pauseParticleSystemsWorld() {
        Iterator var1 = this.systems.iterator();

        while(var1.hasNext()) {
            ((ParticleSystem)var1.next()).pause();
        }

    }

    //INSERTED CODE @53
    public void pauseParticles(Transform transform, int pauseRadius) {
        for(ParticleSystem system : systems) {
            if(pauseRadius > 0) {
                Vector3f systemVector = system.getWorldTransform().origin;
                Vector3f positionVector = transform.origin;
                if(Math.abs(systemVector.lengthSquared() - positionVector.lengthSquared()) <= pauseRadius) system.pause();
            } else {
                if(system.getWorldTransform().origin.equals(transform.origin)) system.pause();
            }
        }
    }

    public void stopParticles(Transform transform, int stopRadius) {
        for(ParticleSystem system : systems) {
            if(stopRadius > 0) {
                Vector3f systemVector = system.getWorldTransform().origin;
                Vector3f positionVector = transform.origin;
                if(Math.abs(systemVector.lengthSquared() - positionVector.lengthSquared()) <= stopRadius) system.stop();
            } else {
                if(system.getWorldTransform().origin.equals(transform.origin)) system.stop();
            }
        }
    }
    //

    public void update(Physics var1, Timer var2) {
        ObjectListIterator var3 = this.systems.iterator();

        while(var3.hasNext()) {
            ParticleSystem var4;
            (var4 = (ParticleSystem)var3.next()).update(var1, var2);
            if (var4.isCompletelyDead()) {
                var4.clearList();
                System.err.println("Completely removed particle system");
                var3.remove();
            }
        }

    }

    public void draw() {
        Particle.quickSort(this);
        Iterator var1 = this.systems.iterator();

        while(var1.hasNext()) {
            ((ParticleSystem)var1.next()).draw();
        }

    }

    public void openGUI(final ClientState var1) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    ParticleSystemGUI var1x = new ParticleSystemGUI(var1);
                    CameraMouseState.ungrabForced = !CameraMouseState.ungrabForced;
                    var1x.setAlwaysOnTop(true);
                    var1x.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
                    var1x.setVisible(true);
                } catch (Exception var2) {
                    var2.printStackTrace();
                }
            }
        });
    }

    //INSERTED CODE @92
    public void openGUI(final ClientState var1, final String[] location) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    if(location.length == 3) {
                        int x = Integer.parseInt(location[0]);
                        int y = Integer.parseInt(location[1]);
                        int z = Integer.parseInt(location[2]);
                        Transform transform = new Transform();
                        transform.set(new Matrix3f());
                        transform.origin.set(x, y, z);
                        ParticleSystemGUI var1x = new ParticleSystemGUI(var1, transform);
                        CameraMouseState.ungrabForced = !CameraMouseState.ungrabForced;
                        var1x.setAlwaysOnTop(true);
                        var1x.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
                        var1x.setVisible(true);
                    }
                } catch (Exception var2) {
                    var2.printStackTrace();
                }
            }
        });
    }
    //

    public float get(int var1) {
        Vector3f var2 = ((ParticleSystem)this.systems.get(var1)).getWorldTransform().origin;
        Matrix4f.transform(Controller.modelviewMatrix, new Vector4f(var2.x, var2.y, var2.z, 1.0F), res);
        return res.z;
    }

    public void switchVal(int var1, int var2) {
        Collections.swap(this.systems, var1, var2);
    }

    public int getSize() {
        return this.systems.size();
    }

    class WorldTransformable implements Transformable {
        TransformTimed transform = new TransformTimed();

        public WorldTransformable(Transform var2) {
            this.transform.set(var2);
        }

        public TransformTimed getWorldTransform() {
            return this.transform;
        }
    }
}