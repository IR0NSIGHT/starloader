package api.utils;

import api.mod.StarMod;
import api.utils.registry.UniversalRegistry;
import org.schema.game.common.controller.PlayerUsableInterface;

public class PlayerUsableHelper {

    /**
     * Gets a PlayerUsableInterface id from mod+uid
     *
     * The blockId will be registered in ICONS if it doesnt exist already.
     * These values persist between restart and are synchronized between client and server on join.
     */
    public static long getPlayerUsableId(short blockId, StarMod container, String uniqueId){
        long urv = UniversalRegistry.getExistingURV(UniversalRegistry.RegistryType.PLAYER_USABLE_ID, container, uniqueId);
        //Might already be registered, but it doesnt matter
        PlayerUsableInterface.ICONS.put(urv, blockId);
        return urv;
    }
}
