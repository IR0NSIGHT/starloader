package api.utils.game.module;

import api.common.GameCommon;
import api.mod.StarMod;
import api.network.packets.PacketUtil;
import it.unimi.dsi.fastutil.longs.Long2ByteOpenHashMap;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.power.reactor.PowerConsumer;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.player.PlayerState;
import org.schema.schine.graphicsengine.core.Timer;

/**
 * Created by Jake on 12/9/2020.
 * <insert description here>
 */
public abstract class ModManagerContainerModule implements PowerConsumer, ByteArrayTagSerializable {
    private float powered;
    public Long2ByteOpenHashMap blocks = new Long2ByteOpenHashMap();
    public SegmentController segmentController;
    private final ManagerContainer<?> managerContainer;
    private StarMod mod;
    private short blockId;
    private boolean singlePlayer;
    private boolean onServer;
    ModManagerContainerModule otherModule;

    public abstract void handle(Timer timer);

    public boolean isOnServer() {
        return onServer;
    }

    public boolean isOnSinglePlayer() {
        return singlePlayer;
    }

    public ModManagerContainerModule(SegmentController ship, ManagerContainer<?> managerContainer, StarMod mod, short blockId) {
        this.segmentController = ship;
        this.managerContainer = managerContainer;
        this.mod = mod;
        this.blockId = blockId;
        this.singlePlayer = GameCommon.isOnSinglePlayer();
        this.onServer = managerContainer.isOnServer();
        //If we are on single player, store a reference to the server
//        if(singlePlayer){
//            //Server module is always loaded before client module.
//            String uniqueId = managerContainer + "_" + getClass().getName();
//            System.err.println("NAME: " + uniqueId);
//            if(onServer){
//                serverModules.put(uniqueId, this);
//            }else{
//                ModManagerContainerModule serverModule = serverModules.get(uniqueId);
//                serverModule.otherModule = this;
//                this.otherModule = serverModule;
//                serverModules.remove(uniqueId);
//            }
//        }
    }

    public ModManagerContainerModule getServerModule() {
        assert singlePlayer && !onServer : "Method called from wrong context, you must only call this on single player, from a CLIENT module.";
        return otherModule;
    }
    public ModManagerContainerModule getClientModule() {
        assert singlePlayer && onServer : "Method called from wrong context, you must only call this on single player, from a SERVER module.";
        return otherModule;
    }

//    private final static HashMap<String, ModManagerContainerModule> serverModules = new HashMap<>();

    public short getBlockId() {
        return blockId;
    }

    public ManagerContainer<?> getManagerContainer() {
        return managerContainer;
    }

    public int getSize(){
        return blocks.size();
    }

    public void handlePlace(long abs, byte orientation){
        long var4 = ElementCollection.getPosIndexFrom4(abs);
        System.err.println("Placed: " + abs);
        this.blocks.add(var4, orientation);
    }

    public void handleRemove(long abs){
        long var3 = ElementCollection.getPosIndexFrom4(abs);
        System.err.println("Removed: " + abs);
        this.blocks.remove(var3);
    }


    @Override
    public boolean isPowerCharging(long l) {
        return true;
    }

    @Override
    public void setPowered(float v) {
        this.powered = v;
    }

    @Override
    public float getPowered() {
        return powered;
    }

    @Override
    public PowerConsumerCategory getPowerConsumerCategory() {
        return PowerConsumerCategory.OTHERS;
    }

    @Override
    public void reloadFromReactor(double v, Timer timer, float v1, boolean b, float v2) {

    }

    @Override
    public boolean isPowerConsumerActive() {
        return true;
    }


    @Override
    public void dischargeFully() {

    }

    public String getTagName(){
        return mod.getSkeleton().getName() + "~" + getName();
    }

    public StarMod getMod() {
        return mod;
    }

    /**
     * Write the data of this module to a buffer and send it to the server
     * Use case: A client modifies a value through a gui
     * Callable ONLY FROM THE CLIENT
     */
    public void syncToServer(){
        PacketBSyncMCModule packet = new PacketBSyncMCModule(this);
        PacketUtil.sendPacketToServer(packet);
    }

    /**
     *
     */
    public void syncToClient(PlayerState state){
        PacketUtil.sendPacket(state, new PacketBSyncMCModule(this));
    }



}
