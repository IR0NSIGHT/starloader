package api.network.packets;

import api.mod.StarLoader;
import api.network.Packet;
import api.network.PacketReadBuffer;
import api.network.PacketWriteBuffer;
import api.utils.game.PlayerUtils;
import api.utils.game.chat.CommandInterface;
import org.schema.game.common.data.chat.prefixprocessors.PrefixProcessorAdminCommand;
import org.schema.game.common.data.player.PlayerState;
import java.io.IOException;

/**
 * Created by Jake on 10/18/2020.
 * <insert description here>
 */
public class PacketCSAdminCommand extends Packet {
    String rawArgs;
    String command;

    public PacketCSAdminCommand(String command, String rawArgs) {
        this.command = command;
        this.rawArgs = rawArgs;
    }
    public PacketCSAdminCommand(){

    }

    @Override
    public void readPacketData(PacketReadBuffer buf) throws IOException {
        rawArgs = buf.readString();
        command = buf.readString();
    }

    @Override
    public void writePacketData(PacketWriteBuffer buf) throws IOException {
        buf.writeString(rawArgs);
        buf.writeString(command);
    }

    @Override
    public void processPacketOnClient() {
    }

    @Override
    public void processPacketOnServer(PlayerState sender) {
        if(sender.isAdmin()) {
            CommandInterface cmd = StarLoader.getCommand(command);
            if(cmd != null) cmd.serverAction(sender, PrefixProcessorAdminCommand.createArgsFromString(rawArgs));
            else throw new IllegalStateException("Command not found");
        } else {
            PlayerUtils.sendMessage(sender, "You must be an admin to perform this command!");
        }
    }
}
