package api.listener.events.block;

import api.listener.events.Event;
import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.world.Segment;

public class SegmentPieceDamageEvent extends Event {

    private SegmentPiece hitBlock;
    private ElementInformation hitBlockInfo;
    private Segment hitSegment;
    private double totalDamage;
    private Damager damager;
    private DamageDealerType damageType;
    private boolean firstHit;
    private boolean destroyed;

    public SegmentPieceDamageEvent(SegmentPiece hitBlock, ElementInformation hitBlockInfo, Segment hitSegment, double totalDamage, Damager damager, DamageDealerType damageType, boolean firstHit, boolean destroyed) {
        this.hitBlock = hitBlock;
        this.hitBlockInfo = hitBlockInfo;
        this.hitSegment = hitSegment;
        this.totalDamage = totalDamage;
        this.damager = damager;
        this.damageType = damageType;
        this.firstHit = firstHit;
        this.destroyed = destroyed;
    }

    public SegmentPiece getHitBlock() {
        return hitBlock;
    }

    public ElementInformation getHitBlockInfo() {
        return hitBlockInfo;
    }

    public Segment getHitSegment() {
        return hitSegment;
    }

    public double getTotalDamage() {
        return totalDamage;
    }

    public Damager getDamager() {
        return damager;
    }

    public DamageDealerType getDamageType() {
        return damageType;
    }

    public boolean isFirstHit() {
        return firstHit;
    }

    public boolean isDestroyed() {
        return destroyed;
    }
}
